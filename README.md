# Reto 1

Prueba de markdown en Bitbucket.

#Link
[Bitbucket - Ulises - Reto_1](https://bitbucket.org/Uce_escamilla/reto_1)

#Italic
*This text will be italic*

#List
* Item 1
* Item 2
  * Item 2a
  * Item 2b

#Image
  ![GitHub Logo](/images/logo.png)
Format: ![Alt Text](url)

#Blockquotes
> We're living the future so
> the present is our past.

#In line code
I think you should use an
`<addr>` element here instead.